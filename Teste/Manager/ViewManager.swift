//
//  ViewManager.swift
//  Teste
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class ViewManager {
    class func prepareWindow(window: UIWindow?) {
        let window = window ?? UIWindow()
        let mainVC = MainViewController()
        let mainNav = MainNavigationController(rootViewController: mainVC)
        window.rootViewController = mainNav
        window.makeKeyAndVisible()
    }
    
    
    class func alert(with title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, cancelAction: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?) {
        
        func showDefault(title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?){
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "OK", style: .default, handler: confirmActionHandler)
            alert.addAction(confirmAction)
            DispatchQueue.main.async{
                viewController.present(alert, animated: true, completion: completion)
            }
        }
        
        func show(with title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, cancelAction: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?){
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Sim", style: .destructive, handler: confirmActionHandler)
            let cancelAction = UIAlertAction(title: "Não", style: .default, handler: cancelAction)
            alert.addAction(confirmAction)
            alert.addAction(cancelAction)
            DispatchQueue.main.async{
                viewController.present(alert, animated: true, completion: completion)
            }
        }
        
        guard let cancelAction = cancelAction else {
            showDefault(title: title, message: message, in: viewController, confirmActionHandler: confirmActionHandler, completion: completion)
            return
        }
        show(with: title, message: message, in: viewController, confirmActionHandler: confirmActionHandler, cancelAction: cancelAction, completion: completion)
    }

}



