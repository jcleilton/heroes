//
//  Validador.swift
//  Teste
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

class Validator {
        
    class func email(email: String) -> [String:Any] {
        if email.isEmpty {
            return [
                "status" : false,
                "message" : "O campo email é requerido."
            ]
        }
        
        if !email.isValidEmail() {
            return [
                "status" : false,
                "message" : "Digite um email válido"
            ]
        }
        
        return [
            "status" : true,
            "message" : ""
        ]
    }
}
