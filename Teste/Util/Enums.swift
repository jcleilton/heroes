//
//  Enums.swift
//  Teste
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

enum FLAVORS {
    case dev
    case prod
}

enum ConnectionSessionHttpMethod{
    case post, get
    func description() -> String{
        switch self {
        case .post: return "POST"
        case .get: return "GET"
        }
    }
}

enum Compression {
    
    /// Fast compression
    case lz4
    
    /// Balanced between speed and compression
    case zlib
    
    /// High compression
    case lzma
    
    /// Apple-specific high performance compression. Faster and better compression than ZLIB, but slower than LZ4 and does not compress as well as LZMA.
    case lzfse
}

enum ConnectionSessionError: Error{
    case invalidUrl, notFound, pushNotificationtokenNotFound, invalidLogin, invalidJSON, userNotLogged, unzipFailed, authTokenNotFound, noResponse
    var localizedDescription: String{
        switch self {
        case .invalidUrl:
            return "URL Inválida"
        case .notFound:
            return "Nenhuma Resposta Encontrada"
        case .pushNotificationtokenNotFound:
            return "Token da Notificação Push não encontrado."
        case .invalidLogin:
            return "Usuário e/ou Senha inválidos."
        case .invalidJSON:
            return "O objeto JSON não pôde ser lido."
        case .userNotLogged:
            return "Usuário deve estar logado."
        case .unzipFailed:
            return "Falha ao realizar o unzip."
        case .authTokenNotFound:
            return "Token de autenticação não encontrado."
        case .noResponse:
            return "Sem resposta do servidor."
        }
    }
    
    func description() -> String{
        switch self {
        case .invalidUrl:
            return "URL Inválida"
        case .notFound:
            return "Nenhuma Resposta Encontrada"
        case .pushNotificationtokenNotFound:
            return "Token da Notificação Push não encontrado."
        case .invalidLogin:
            return "Usuário e/ou Senha inválidos."
        case .invalidJSON:
            return "O objeto JSON não pôde ser lido."
        case .userNotLogged:
            return "Usuário deve estar logado."
        case .unzipFailed:
            return "Falha ao realizar o unzip."
        case .authTokenNotFound:
            return "Token de autenticação não encontrado."
        case .noResponse:
            return "Sem resposta do servidor."
        }
    }

}
