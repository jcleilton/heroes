//
//  Constantes.swift
//  Teste
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class Constants {
    let flavor: FLAVORS = {
        #if DEV
        return FLAVORS.dev
        #else
        return FLAVORS.prod
        #endif
    }()
    
    //Criei um serviço em node js para login e recuperação de senha. Devido ao pouco tempo, não fiz o CRUD completo da aplicação.
    lazy var rootUrl: String = { [weak self] in
        guard let me = self else { return "http://192.168.0.101:3000" }
        if (me.flavor == .dev) {
            return "http://192.168.0.101:3000"
        }
        return "https://pure-fortress-43101.herokuapp.com"
    }()
    
    lazy var mainColor: UIColor = { [weak self] in
        guard let me = self else { return .white }
        return UIColor(red: 140/255, green: 25/255, blue: 32/255, alpha: 1)
    }()
    
    lazy var secondyColor: UIColor = { [weak self] in
        guard let me = self else { return .white }
        return UIColor(red: 213/255, green: 2512/255, blue: 210/255, alpha: 1)
    }()
    
}
