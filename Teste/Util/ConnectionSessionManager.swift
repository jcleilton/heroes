//
//  ConnectionSessionManager.swift
//  Teste
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit


struct ConnectionSessionManager {
    typealias Callback<T: Any, E: Any> = (T, E) -> Void
    static var session = URLSession.shared
    static let constants = Constants()
    
    static func invoke(url: String, withArgs args: Dictionary<String, Any>, httpMethod: ConnectionSessionHttpMethod, completion: Callback<Any?, NSError?>?){
        URLCache.shared.removeAllCachedResponses()
        guard let url = URL(string: url) else{
            completion?(nil, ConnectionSessionError.invalidUrl as NSError?)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.description()
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if args.count > 0 {
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: args, options: .prettyPrinted)
            }catch{
                
                completion?(nil, error as NSError?)
            }
        }
        
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let error = error else{
                guard let data = data else {
                    completion?(nil, NSError())
                    return
                }
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableLeaves])
                    completion?(json, nil)
                    return
                }catch{
                    completion?(nil, error as NSError?)
                    return
                }
            }
            completion?(nil, error as NSError?)
        }).resume()
    }
    
    
    static func downloadImage(from url: URL, completion: @escaping Callback<UIImage?, Error?>){
        URLCache.shared.removeAllCachedResponses()
        session.dataTask(with: url, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                guard let error = error else{
                    guard let data = data else{
                        return completion(nil, nil)
                    }
                    let image = UIImage(data: data)
                    return completion(image, nil)
                }
                completion(nil, error)
            }
        }).resume()
    }
    
    
    static func simulatarRequisicao(completion: @escaping() -> Void) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: completion)
    }
   
}
