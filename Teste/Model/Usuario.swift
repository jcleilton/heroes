//
//  Usuario.swift
//  Teste
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation
import CoreData

fileprivate var usuario: Usuario?

protocol UsuarioDelegate: class {
    func finalizandoRequest(usuario: Usuario)
    func falhaRequest(message: String)
}

class Usuario {
    
    var id: Int = 0
    var nome: String?
    var email: String?
    var token: String?
    
    var usuarioData: UsuarioData?
    
    weak var delegate: UsuarioDelegate?
    static var shared: Usuario? = {
        return usuario
    }()
    
    init(
        id: Int,
        nome: String?,
        email: String?,
        token: String?
    ) {
        self.id = id
        self.nome = nome
        self.email = email
        self.token = token
        DispatchQueue.main.async { [weak self] in
            self?.saveData()
        }
    }
    
    convenience init(dict: [String:Any], token: String) {
        let nome = dict["nome"] as? String
        let email = dict["email"] as? String
        let id = dict["id"] as? Int ?? 0
        self.init(
            id: id,
            nome: nome,
            email: email,
            token: token
        )
    }
    
    func create() {
        usuario = self
    }
    
    func saveData() {
        getContext { [weak self] context in
            guard let me = self else { return }
            if let context = context {
                if let usuarioData = me.usuarioData {
                    usuarioData.id = Int32(me.id)
                    usuarioData.nome = me.nome
                    usuarioData.email = me.email
                    usuarioData.token = me.token
                    do {
                        try context.save()
                    } catch {
                        print(error.localizedDescription)
                    }
                } else {
                    Usuario.getUser(id: Int32(me.id), completion: { (data) in
                        if let usuarioData = data {
                            usuarioData.id = Int32(me.id)
                            usuarioData.nome = me.nome
                            usuarioData.email = me.email
                            usuarioData.token = me.token
                            do {
                                try context.save()
                            } catch {
                                print(error.localizedDescription)
                            }
                        } else {
                            let usuarioData = UsuarioData(context: context)
                            usuarioData.id = Int32(me.id)
                            usuarioData.nome = me.nome
                            usuarioData.email = me.email
                            usuarioData.token = me.token
                            do {
                                try context.save()
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    })
                }
            }
        }
        
    }
    
    func salvarCredenciasParaLoginSilencioso() {
        let email = self.email ?? ""
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.set(true, forKey: "loginSilencioso")
        UserDefaults.standard.synchronize()
    }
    
    class func getEmailLoginSilencioso() -> String {
        let loginSilencioso = UserDefaults.standard.value(forKey: "loginSilencioso") as? Bool ?? false
        let email = getUltimoEmailLogado()
        return loginSilencioso ? email : ""

    }
    
    class func getUltimoEmailLogado() -> String {
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else{
            return ""
        }
        return email
    }
    
    class func recuperarUltimoUsuario(completion: @escaping(Bool) -> Void) {
        let email = Usuario.getEmailLoginSilencioso()
        if email.isValidEmail() {
            //visto que o serviço não prevê token refresh, to implementando, por enquanto, sem token e logando direto, mas sendo necessário melhorar o serviço, salvar o token e usar o token refresh
            Usuario.getUser(email: email, completion: { user in
                let usuario = Usuario(id: Int(user?.id ?? 0), nome: user?.nome, email: user?.email, token: "")
                usuario.usuarioData = user
                usuario.create()
                completion(true)
            })
        } else {
            completion(false)
        }
    }
    
    func deleteData() {
        getContext { context in
            if let context = context {
                if let usuarioData = self.usuarioData {
                    context.delete(usuarioData)
                }
            }
        }
        
    }
    
    
    
    func getUser() -> UsuarioData? {
        return self.usuarioData
    }
    
    public class func userFetchRequest() -> NSFetchRequest<UsuarioData> {
        return NSFetchRequest<UsuarioData>(entityName: "UsuarioData")
    }
    
    
    class func getUser(id: Int32, completion: @escaping(UsuarioData?) -> Void) {
        if id == 0 {
            return completion(nil)
        }
        let request = userFetchRequest()
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        request.returnsObjectsAsFaults = false
        getContext { (context) in
            if let context = context {
                do {
                    let result = try context.fetch(request).first
                    return completion(result)
                } catch {
                    print(error.localizedDescription)
                    return completion(nil)
                }
            } else {
                return completion(nil)
            }
        }
    }
    
    class func getUser(email: String, completion: @escaping(UsuarioData?) -> Void) {
        if !email.isValidEmail() {
            return
        }
        let request = userFetchRequest()
        request.predicate = NSPredicate(format: "email = %@", email)
        request.returnsObjectsAsFaults = false
        getContext { (context) in
            if let context = context {
                do {
                    let result = try context.fetch(request).first
                    return completion(result)
                } catch {
                    print(error.localizedDescription)
                    return completion(nil)
                }
            } else {
                return completion(nil)
            }
        }
    }
    
    
    class func isLogged() -> Bool {
        guard let token = Usuario.shared?.token else {
            return false
        }
        return token.count > 0
    }
    
    func logout() {
        usuario = nil
        UserDefaults.standard.set(false, forKey: "loginSilencioso")
    }

}
