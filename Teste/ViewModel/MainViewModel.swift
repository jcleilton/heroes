//
//  MainViewModel.swift
//  Teste
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

protocol MainViewModelDelegate: class {
}

class MainViewModel {
    weak var delegate: MainViewModelDelegate?
    
    func fetch(
        value: String,
        offset: Int
    ) {
        
    }
    
    
}

