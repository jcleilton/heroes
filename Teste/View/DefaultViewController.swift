//
//  DefaultViewController.swift
//  Teste
//
//  Created by José C Feitosa on 11/02/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

// ViewController que será mãe de todas as viewcontrollers
class DefaultViewController: UIViewController {
    
    var activityController: UIView? = nil
    var labelActivity: UILabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 200.0, height: 30.0))
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    weak var constraintButton: NSLayoutConstraint?
    
    var initialValueOfConstraintButton:CGFloat = 0
    
    let constantes = Constants()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        } else {
        }
    }
    
    func setupNavigation(_ title: String? = nil) {
        self.navigationItem.title = title ?? "Heroes"
//        self.navigationItem.titleView?.tintColor = constantes.mainViewStruct.navBarTextColor
//        self.navigationItem.leftBarButtonItem?.tintColor = constantes.mainViewStruct.navBarTextColor
//        self.navigationItem.rightBarButtonItem?.tintColor = constantes.mainViewStruct.navBarTextColor
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showActivity(){
        if activityController == nil {
            activityController = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
            activityController?.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
            let activity = UIActivityIndicatorView(style: .whiteLarge)
            activity.center = (activityController?.center)!
            activity.startAnimating()
            
            labelActivity.frame.size.width = (activityController?.bounds.width ?? 0.0) * 0.9
            labelActivity.center = (activityController?.center)!
            labelActivity.frame.origin.x = ((activityController?.bounds.width ?? 0.0) - labelActivity.bounds.width)/2
            labelActivity.frame.origin.y = activity.frame.origin.y + activity.bounds.height + 8
            labelActivity.text = "Carregando..."
            labelActivity.textAlignment = .center
            labelActivity.textColor = UIColor.white
            
            activityController?.addSubview(labelActivity)
            activityController?.addSubview(activity)
        }
        guard let navigationController = self.navigationController else {
            self.view.addSubview(self.activityController!)
            return
        }
        navigationController.view.addSubview(activityController!)
    }
    
    func hideActivity(){
        activityController?.removeFromSuperview()
    }
    
    func registerForKeyboardNotification(initialValueForConstraint: CGFloat){
        self.initialValueOfConstraintButton = initialValueForConstraint
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: Notification.Name("UIKeyboardWillHideNotification"), object: nil)
    }
    
    @objc func keyboardWasShown(_ notification: Notification) {
        if constraintButton != nil {
            if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                constraintButton?.constant = (keyboardHeight + 10) * -1
                self.view.updateConstraints()
            }
        }
    }
    
    @objc func keyboardWillBeHidden() {
        if constraintButton != nil {
            constraintButton?.constant = initialValueOfConstraintButton
            self.view.updateConstraints()
        }
    }
    
}

