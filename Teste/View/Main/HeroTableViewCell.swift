//
//  PautaUITableViewCell.swift
//  Teste
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit


protocol HeroCellDelegate: class {
    
}

class HeroTableViewCell: UITableViewCell {
    
    weak var delegate: HeroCellDelegate?
    
    lazy var view: UIView = {
        let obj = UIView()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.clipsToBounds = true
        obj.layer.cornerRadius = 12
        obj.layer.borderColor = UIColor.darkGray.cgColor
        obj.layer.borderWidth = 0.4
        obj.backgroundColor = .white
        return obj
    }()
    
    lazy var viewShadow: UIView = {
        let obj = UIView()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.clipsToBounds = true
        obj.layer.cornerRadius = 12
        obj.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        return obj
    }()
    
    
    
    lazy var titleLabel: UILabel = {
        let obj = UILabel()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.textColor = .blue
        obj.textAlignment = .center
        obj.font = UIFont(name: obj.font.fontName, size: 20)
        obj.numberOfLines = 0
        obj.lineBreakMode = .byWordWrapping
        return obj
    }()
    
    lazy var descLabel: UILabel = {
        let obj = UILabel()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.textColor = UIColor.darkGray
        obj.textAlignment = .justified
        obj.font = UIFont(name: obj.font.fontName, size: 16)
        obj.numberOfLines = 0
        obj.lineBreakMode = .byWordWrapping
        
        return obj
    }()
    
    
    
    
    
    
    var index = 0
    
    override func didMoveToSuperview() {
        self.setupCell()
    }
    
    @objc func touchOnView() {
        
    }
    
    private func setupCell() {
        self.contentView.backgroundColor = .clear
        
        self.contentView.addSubview(self.viewShadow)
        self.contentView.addSubview(self.view)
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.descLabel)
        
        
        self.view.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20).isActive = true
        self.view.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20).isActive = true
        self.view.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20).isActive = true
        self.view.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -20).isActive = true
        
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.touchOnView)))
        
        
        self.viewShadow.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 3).isActive = true
        self.viewShadow.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 3).isActive = true
        self.viewShadow.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 2).isActive = true
        self.viewShadow.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 2).isActive = true
        
        
        self.titleLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 10).isActive = true
        self.titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.titleLabel.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.9).isActive = true
        self.titleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.descLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 3).isActive = true
        self.descLabel.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
        self.descLabel.rightAnchor.constraint(equalTo: self.titleLabel.rightAnchor).isActive = true
        self.descLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        self.layoutIfNeeded()
        self.updateConstraints()
        
    }
    
    func setupCell(_ data: Hero) {
        DispatchQueue.main.async {
            
        }
    }
    
    @objc func okAction() {
        
    }
    

}
