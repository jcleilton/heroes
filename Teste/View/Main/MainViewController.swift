//
//  MainViewController.swift
//  Teste
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class MainViewController: DefaultViewController {
    
    lazy var searchBar: UISearchBar = { [weak self] in
        let obj = UISearchBar()
        obj.translatesAutoresizingMaskIntoConstraints = false
        guard let me = self else { return obj }
        obj.backgroundImage = UIImage()
        let colorText = UIColor.red
        let colorBack = UIColor.white
        obj.setTextColor(color: colorText)
        obj.setSearchImageColor(color: colorBack)
        
        obj.scopeButtonTitles = ["Abertas","Finalizadas"]
        obj.selectedScopeButtonIndex = 0
        
        
        
        guard let textFieldInsideSearchBar = obj.getSearchBarTextField() else{
            return obj
        }
        let pointSize = textFieldInsideSearchBar.font!.pointSize
        textFieldInsideSearchBar.layer.cornerRadius = 10.0
        textFieldInsideSearchBar.attributedPlaceholder =  NSAttributedString(string: "Buscar", attributes: [.foregroundColor: colorText])

        return obj
    }()
    
    lazy var tableView: UITableView = {
        let tbv = UITableView()
        tbv.translatesAutoresizingMaskIntoConstraints = false
        tbv.separatorStyle = .none
        tbv.backgroundColor = .clear
        return tbv
    }()
    
    lazy var label: UILabel = {
        let obj = UILabel()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.textColor = .darkGray
        obj.text = "Sem dados"
        return obj
    }()
    

    var viewModel = MainViewModel()
    
    var selectedIndex = -1 {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigation("Heroes")
        self.setupNavButtons()
        self.view.backgroundColor = .white
        self.setupView()
        
        self.searchBar.delegate = self
        
        self.tableView.register(HeroTableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.viewModel.delegate = self
        self.resetData()
    }
    
    func setupView() {
        self.view.addSubview(self.searchBar)
        self.searchBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.searchBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.searchBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        self.view.addSubview(self.tableView)
        self.tableView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.searchBar.leadingAnchor).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.searchBar.trailingAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.view.addSubview(self.label)
        self.label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.label.centerYAnchor.constraint(equalToSystemSpacingBelow: self.view.centerYAnchor, multiplier: 0.8).isActive = true
        
        self.view.layoutIfNeeded()
    }
    
    func setupNavButtons() {
        
    }
    
    func resetData(value: String = "", offset: Int = 0) {
        self.viewModel.fetch(value: value, offset: offset)
    }
    
    
}


extension MainViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.resetData()
        searchBar.text = ""
        searchBar.selectedScopeButtonIndex = 0
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsScopeBar = false
        searchBar.showsCancelButton = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.resetData(value: searchBar.text ?? "", offset: 0)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsScopeBar = true
        searchBar.showsCancelButton = true
        
    }
    
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0//self.cellStructs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? HeroTableViewCell {
            cell.setupCell(Hero())
            cell.delegate = self
            cell.index = indexPath.row
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}


extension MainViewController: MainViewModelDelegate, HeroCellDelegate {
    
}
