//
//  MainNavigationController.swift
//  Teste
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    let constants = Constants()
    
    lazy var debugIndicatorLabel: UILabel = {
        let obj = UILabel()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.text = "DEBUG"
        obj.textColor = UIColor.yellow
        return obj
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        } else {
        }
        self.setup()
    }
    
    func setup() {
        if constants.flavor == .dev {
            self.navigationBar.addSubview(self.debugIndicatorLabel)
            self.debugIndicatorLabel.topAnchor.constraint(equalTo: self.navigationBar.topAnchor, constant: 20).isActive = true
            self.debugIndicatorLabel.trailingAnchor.constraint(equalTo: self.navigationBar.trailingAnchor, constant: -15).isActive = true
        }
        self.navigationBar.barTintColor = constants.mainColor
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: constants.secondyColor]
    }

}
